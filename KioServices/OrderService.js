'use strict'

const db = require('../models')
const axios = require('axios')
const { buildKioQuery } = require('../helpers/queryHelper')

const URL = '/orders'

async function getAll(params) {
  const query = buildKioQuery(params)
  try {
    const kioRes = await axios.get(URL, {
      params: query
    })
    if (kioRes.status === 200) {
      return {
        rows: kioRes.data.data,
        count: kioRes.data.total
      }
    }
    throw new Error('Error call api Kio')
  } catch (error) {
    console.log(error);
    return { rows: [], count: 0 }
  }
}

async function getById(customerId) {
  try {
    const kioRes = await axios.get(`${URL}/${customerId}`)
    if (kioRes.status === 200) {
      return kioRes.data
    }
    throw new Error('Error call api Kio')
  } catch (error) {
    console.log(error);
    return null;
  }
}

async function create(data) {
  try {
    const order = await db.Order.findOne({
      where: {
        id: data.id,
        isDeleted: false
      },
      include: [
        {
          model: db.OrderProduct, include: [
            { model: db.Product }
          ]
        },
        { model: db.Member }
      ]
    })
    const postData = {}
    if (order) {
      postData.branchId = 154181
      postData.discount = 0
      postData.description = ''
      postData.method = ''
      postData.customerId = order.Member.customerId
      postData.totalPayment = order.totalAmount
      postData.orderDetails = order.OrderProducts.map(orderProduct => ({
        productId: orderProduct.Product.productId,
        productCode: orderProduct.Product.code,
        productName: orderProduct.Product.name,
        quantity: orderProduct.quantity,
        price: orderProduct.price,
      }))
      if (order.member) {
        inserted.customer = {
          id: order.member.customerId,
          name: order.member.fullName,
          contactNumber: order.member.phone[0]
        }
      }
      console.log(postData);
      const kioRes = await axios.post(URL, postData)
      console.log(kioRes.status);
      if (kioRes.status === 200) {
        return kioRes.data
      }
      throw new Error('Error call api Kio')
    }
    throw new Error('Not found Order')
  } catch (error) {
    console.log(error.message);
    return null;
  }
}

module.exports = {
  getAll,
  getById,
  create,
}