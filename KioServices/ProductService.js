'use strict'

const axios = require('axios')
const { buildKioQuery } = require('../helpers/queryHelper')

const URL = '/products'

async function getAll(params) {
  const query = buildKioQuery(params)
  try {
    const kioRes = await axios.get(URL, {
      params: query
    })
    if (kioRes.status === 200) {
      return {
        rows: kioRes.data.data,
        count: kioRes.data.total
      }
    }
    throw new Error('Error call api Kio')
  } catch (error) {
    console.log(error);
    return { rows: [], count: 0 }
  }
}

async function getById(productId) {
  try {
    const kioRes = await axios.get(`${URL}/${productId}`)
    if (kioRes.status === 200) {
      return kioRes.data
    }
    throw new Error('Error call api Kio')
  } catch (error) {
    console.log(error);
    return null;
  }
}

module.exports = {
  getAll,
  getById,
}