'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const upload = require('express-fileupload')
const { setupKioAxios } = require('./config/axios')
const KioCronjob = require('./cronjobs/KioCronjob')
const requestio = require('./middleware/requestio')
const routes = require('./routes')

const app = express()
app.set('host', process.env.NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(upload());

const KioJob = new KioCronjob()
KioJob.run()
let token
let retry = setInterval(() => {
  token = KioJob.getToken()
  if (!token) {
    KioJob.setToken()
  } else {
    setupKioAxios(token);
    clearInterval(retry)
  }
}, 1000);

app.use(requestio)
app.use(routes)

app.listen(app.get('port'), () => console.log(`Sever listening on port ${app.get('port')}!`))