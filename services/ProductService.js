'use strict'

const { Op } = require('sequelize')
const db = require('../models')

function getAll(params) {
  return db.Product.findAndCountAll(params)
}

function getOne(params) {
  return db.Product.findOne({ where: { ...params, isDeleted: false } })
}

function getById(id) {
  return db.Product.findByPk(id)
}

function create(data) {
  return db.Product.create(data)
}

function update(id, data, transaction) {
  return db.Product.update(data, {
    where: {
      id,
      isDeleted: false
    }
  })
}

function getAllByQuantity(params) {
  params.where.isActive = true
  params.where.basePrice = {
    [Op.gt]: 0
  }
  const include = [{
    model: db.ProductQuantity,
    require: true,
    where: {
      quantity: {
        [Op.gt]: 0
      }
    },
    attributes: ['quantity'],
    order: ['quantity', 'DESC']
  }]
  if (params.params.categoryId) {
    include.push({
      model: db.ProductProductCategory,
      require: true,
      where: { productCategoryId: params.params.categoryId },
      attributes: []
    })
  }
  params.include = include
  params.order = []
  if (params.params.sort === 'priceHighest') {
    params.order.push(['basePrice', 'DESC'])
  }
  else if (params.params.sort === 'priceLowest') {
    params.order.push(['basePrice', 'ASC'])
  }
  params.order.push(['updatedAt', 'DESC'])
  return db.Product.findAndCountAll(params)
}

function getAllBySale(params) {
  params.where.isActive = true
  params.where.allowSale = true
  params.where.salePrice = {
    [Op.gt]: 0
  }
  params.include = [{
    model: db.ProductQuantity,
    require: true,
    where: {
      quantity: {
        [Op.gt]: 0
      }
    },
    attributes: ['quantity'],
    order: ['quantity', 'DESC']
  }]
  params.order = [['saleValue', 'DESC']]
  return db.Product.findAndCountAll(params)
}

module.exports = {
  getAll,
  getOne,
  getById,
  create,
  update,
  getAllByQuantity,
  getAllBySale,
}