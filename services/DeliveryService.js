'use strict'

const db = require('../models')

function getAll(params) {
  delete params.where.isDeleted
  return db.Delivery.findAndCountAll(params)
}

function getOne(params) {
  return db.Delivery.findOne({ where: params })
}

function getById(id) {
  return db.Delivery.findByPk(id)
}

function create(data) {
  return db.Delivery.create(data)
}

function update(id, data) {
  return db.Delivery.update(data, {
    where: {
      id
    }
  })
}

function countByMemberId(memberId) {
  return db.Delivery.count({ where: { memberId } })
}

module.exports = {
  getAll,
  getOne,
  getById,
  create,
  update,
  countByMemberId,
}