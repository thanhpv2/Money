'use strict'

const KioProductService = require(`../KioServices/ProductService`)
const KioProductCategoryService = require(`../KioServices/ProductCategoryService`)
const KioCustomerService = require(`../KioServices/CustomerService`)
const KioOrderService = require(`../KioServices/OrderService`)
const ProductService = require(`../services/ProductService`)
const ProductCategoryService = require(`../services/ProductCategoryService`)
const ProductQuantityService = require(`../services/ProductQuantityService`)
const ProductProductCategoryService = require(`../services/ProductProductCategoryService`)
const MemberService = require(`../services/MemberService`)
const OrderService = require(`../services/OrderService`)
const Validation = require('../validations')
const Util = require('../utils')

var _totalProducts = 0
var _totalCategories = 0
var _totalCustomers = 0
var _totalOrders = 0
var _processingProducts = 0

async function fetchKiotProducts(limit = 100, page = 1) {
  const { rows, count } = await KioProductService.getAll({ limit, page })
  if (rows.length) {
    await migrateProducts(rows)
    _totalProducts += rows.length
    console.log(_totalProducts);
    if (_totalProducts < count) {
      await fetchKiotProducts(limit, page + 1)
    }
  }
  return _totalProducts
}

function migrateProducts(products) {
  return Promise.all(products.map(async product => upsertProduct(product)))
}

async function upsertProduct(product) {
  console.log(product.images);
  const exist = await ProductService.getOne({ productId: product.id })
  const quantity = product.inventories && product.inventories.length ? product.inventories.reduce((a, b) => a + b.onHand, 0) : 0
  if (exist) {
    const updatedProduct = await ProductService.update(exist.id, {
      name: product.name,
      code: product.code,
      description: product.description,
      allowSale: product.allowSale,
      basePrice: product.basePrice,
      images: product.images || [],
      isActive: product.isActive,
    })
    if (updatedProduct) {
      // console.log('updatedProduct', exist.id, _processingProducts);
      _processingProducts++
    }
    await ProductQuantityService.upsert(exist.id, parseInt(quantity))
    let category = await ProductCategoryService.getOne({ categoryId: product.categoryId })
    if (category) {
      const exitRelation = await ProductProductCategoryService.getOne({ productId: exist.id, productCategoryId: category.id })
      if (!exitRelation) {
        ProductProductCategoryService.create({ productId: exist.id, productCategoryId: category.id })
      }
    }
  }
  else {
    const createdProduct = await ProductService.create({
      productId: product.id,
      name: product.name,
      code: product.code,
      description: product.description,
      allowSale: product.allowSale,
      basePrice: product.basePrice,
      image: product.images,
      isActive: product.isActive,
    })
    if (createdProduct) {
      // console.log('createdProduct', createdProduct.id, _processingProducts);
      _processingProducts++
    }
    await ProductQuantityService.upsert(createdProduct.id, parseInt(quantity))
    let category1 = await ProductCategoryService.getOne({ categoryId: product.categoryId })
    if (category1) {
      ProductProductCategoryService.create({ productId: createdProduct.id, productCategoryId: category1.id })
    }
  }
}

async function fetchKiotCategories(limit = 100, page = 1) {
  const { rows, count } = await KioProductCategoryService.getAll({ limit, page })
  if (rows.length) {
    await migrateCategories(rows)
    _totalCategories += rows.length
    console.log(_totalCategories);
    if (_totalCategories < count) {
      await fetchKiotCategories(limit, page + 1)
    }
  }
  return _totalCategories
}

function migrateCategories(categories) {
  return Promise.all(categories.map(category => upsertCategory(category)))
}

async function upsertCategory(category) {
  const exist = await ProductCategoryService.getOne({ categoryId: category.categoryId })
  if (exist) {
    return await ProductCategoryService.update(exist.id, {
      name: category.categoryName,
      isActive: category.isActive,
    })
  }
  else {
    return await ProductCategoryService.create({
      categoryId: category.categoryId,
      name: category.categoryName,
      isActive: category.isActive,
      updatedAt: category.modifiedDate,
      createdAt: category.createdDate,
    })
  }
}

async function fetchKiotCustomers(limit = 100, page = 1) {
  const { rows, count } = await KioCustomerService.getAll({ limit, page })
  if (rows.length) {
    await migrateCustomers(rows)
    _totalCustomers += rows.length
    console.log(_totalCustomers);
    if (_totalCustomers < count) {
      await fetchKiotCustomers(limit, page + 1)
    }
  }
  return _totalCustomers
}

function migrateCustomers(customers) {
  return Promise.all(customers.map(customer => upsertCustomer(customer)))
}

async function upsertCustomer(customer) {
  const exist = await MemberService.getOne({ customerId: customer.id })
  const phone = customer.contactNumber ? customer.contactNumber.split('/').map(p => Util.convertPhoneTo0x(p)).filter(p => Validation.isPhone(p)) : []
  if (exist) {
    return await MemberService.update(exist.id, {
      fullName: customer.name,
      phone,
      email: customer.email,
      address: customer.address,
      referralCode: customer.code,
    })
  }
  else {
    if (phone.length) {
      return await MemberService.create({
        customerId: customer.id,
        username: phone[0],
        password: phone[0],
        fullName: customer.name,
        phone,
        email: customer.email,
        referralCode: customer.code,
        updatedAt: customer.modifiedDate,
        createdAt: customer.createdDate,
      })
    }
  }
}

async function fetchKiotOrders(limit = 100, page = 1) {
  const { rows, count } = await KioOrderService.getAll({ limit, page })
  if (rows.length) {
    await migrateOrders(rows)
    _totalOrders += rows.length
    console.log(_totalOrders);
    if (_totalOrders < count) {
      await fetchKiotOrders(limit, page + 1)
    }
  }
  return _totalOrders
}

function migrateOrders(orders) {
  return Promise.all(orders.map(order => upsertOrder(order)))
}

async function upsertOrder(order) {
  const exist = await OrderService.getOne({ orderId: order.id })
  if (!exist) {
    const customer = await MemberService.getOne({ customerId: order.customerId })
    if (customer) {
      const data = {
        orderId: order.id,
        code: order.code,
        customerId: customer.id,
        countProduct: order.orderDetails.length,
        totalAmount: order.total,
        updatedAt: order.modifiedDate || order.createdDate,
        createdAt: order.createdDate,
      }
      if (order.orderDetails) {
        data.products = await Promise.all(order.orderDetails.map(async detail => {
          const product = await ProductService.getOne({ productId: detail.productId })
          return {
            productId: product.id,
            basePrice: product.basePrice,
            price: detail.price,
            quantity: parseInt(detail.quantity),
            discount: detail.discount,
            amount: detail.price * detail.quantity
          }
        }))
      }
      return await OrderService.create(data)
    }
  }
}

module.exports = {
  fetchKiotProducts,
  fetchKiotCategories,
  fetchKiotCustomers,
  fetchKiotOrders,
}