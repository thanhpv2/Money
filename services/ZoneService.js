'use strict'

const db = require('../models')

function getProvinces(params) {
  params.where.level = 1
  params.order = [['position', 'ASC']]
  delete params.where.isDeleted
  return db.Zone.findAndCountAll(params)
}

function getDistrictByProvinceId(params) {
  params.where.level = 2
  params.order = [['position', 'ASC']]
  delete params.where.isDeleted
  return db.Zone.findAndCountAll(params)
}

function getById(id) {
  return db.Zone.findByPk(id)
}

module.exports = {
  getProvinces,
  getDistrictByProvinceId,
  getById,
}