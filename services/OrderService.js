'use strict'

const { buildQuery } = require('../helpers/queryHelper')
const db = require('../models')

function getAll(params) {
  const condition = buildQuery(params)
  condition.include = [
    { model: db.Member, require: true, attributes: ['fullName', 'phone', 'email'] }
  ]
  return db.Order.findAndCountAll(condition)
}

function getOne(params) {
  return db.Order.findOne({ where: { ...params, isDeleted: false } })
}

function getById(id) {
  return db.Order.findByPk(id)
}

async function create(data) {
  const t = await db.sequelize.transaction();
  try {
    const orderCreated = await db.Order.create({ ...data, countProduct: data.products.length }, { transaction: t })
    if (data.products) {
      await Promise.all(data.products.map(product => db.OrderProduct.create({ ...product, orderId: orderCreated.id, productId: product.id }, { transaction: t })))
    }
    t.commit()
    return orderCreated
  } catch (error) {
    console.log(error);
    t.rollback()
    return null
  }
}

function update(id, data, transaction) {
  const options = {}
  if (transaction) {
    options.transaction = options
  }
  return db.Order.update(data, {
    where: {
      id,
      isDeleted: false
    }
  }, options)
}

module.exports = {
  getAll,
  getOne,
  getById,
  create,
  update,
}