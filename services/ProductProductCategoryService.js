'use strict'

const db = require('../models')

function getOne(params) {
  return db.ProductProductCategory.findOne({ where: params })
}

function create(data) {
  return db.ProductProductCategory.create(data)
}

function remove(id) {
  return db.ProductProductCategory.deleteOne({ where: { id } })
}

module.exports = {
  getOne,
  create,
  remove,
}