'use strict'

const db = require('../models')

function getAll(params) {
  return db.Member.findAndCountAll(params)
}

function getOne(params) {
  return db.Member.findOne({ where: { ...params, isDeleted: false } })
}

function getById(id) {
  return db.Member.findByPk(id)
}

async function create(data) {
  if (data.password) {
    data.passwordHash = data.password
  }
  return db.Member.create(data)
}

function update(id, data) {
  return db.Member.update(data, {
    where: {
      id,
      isDeleted: false
    }
  })
}

module.exports = {
  getAll,
  getOne,
  getById,
  create,
  update,
}
