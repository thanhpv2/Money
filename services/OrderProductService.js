'use strict'

const db = require('../models')

function getAll(params) {
    return db.OrderProduct.findAndCountAll(params)
}

function getOne(params) {
    return db.OrderProduct.findOne({ where: { ...params, isDeleted: false } })
}

function getById(id) {
    return db.OrderProduct.findByPk(id)
}

function create(data, transaction) {
    const options = {}
    if (transaction) {
        options.transaction = options
    }
    return db.OrderProduct.create(data, options)
}

function update(id, data, transaction) {
    const options = {}
    if (transaction) {
        options.transaction = options
    }
    return db.OrderProduct.update(data, {
        where: {
            id,
        }
    }, options)
}

module.exports = {
    getAll,
    getOne,
    getById,
    create,
    update,
}