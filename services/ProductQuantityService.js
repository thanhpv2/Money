'use strict'

const db = require('../models')

function getOne(params) {
  return db.ProductQuantity.findOne({ where: params })
}

function create(data) {
  return db.ProductQuantity.create(data)
}

function update(productId, quantity) {
  return db.ProductQuantity.update({ quantity }, { where: { productId } })
}

function upsert(productId, quantity) {
  return db.ProductQuantity.upsert({ productId, quantity }, { returning: false })
}

module.exports = {
  getOne,
  create,
  update,
  upsert,
}