const jwt = require('jsonwebtoken')

function encodeData(data) {
  const access_token = jwt.sign(data,
    process.env.JWT_TOKEN_SECRET, {
    expiresIn: process.env.JWT_TOKEN_EXPIRATION * 1000,
  });
  const refresh_token = jwt.sign(data,
    process.env.JWT_REFRESH_TOKEN_SECRET, {
    expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRATION * 1000,
  });
  return {
    access_token,
    exp: process.env.JWT_TOKEN_EXPIRATION,
    refresh_token,
    token_type: 'Bearer'
  }
}

module.exports = {
  encodeData
}
