'use strict'

const db = require('../models')

function getAll(params) {
  return db.ProductCategory.findAndCountAll(params)
}

function getOne(params) {
  return db.ProductCategory.findOne({ where: { ...params, isDeleted: false } })
}

function create(data) {
  return db.ProductCategory.create(data)
}

function update(id, data) {
  return db.ProductCategory.update(data, {
    where: {
      id,
      isDeleted: false
    }
  })
}

function getById(id) {
  return db.ProductCategory.findByPk(id)
}

module.exports = {
  getAll,
  getOne,
  create,
  update,
  getById,
}