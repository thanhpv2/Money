const errorCode = {
    '000': 'Success',
    400: 'invalid input',
    401: 'input missing',
    402: 'input incorrect',
    403: 'Dữ liệu không trùng khớp',
    404: 'Dữ liệu vượt quá giới hạn cho phép',
    500: 'Tham số không hợp lệ',
    501: 'Thiếu tham số đầu vào'
}

class Util {

    constructor() {
        this.status = null;
        this.data = null;
        this.message = null;
    }

    success(data, message, status) {

    }

    set(statusCode, data, options) {
        this.statusCode = statusCode;
        this.message = errorCode[statusCode] || 'Lỗi không xác định';
        this.data = data;
        this.options = options;
    }

    send(res) {
        let result = {
            code: this.statusCode,
            message: this.message,
            ...this.options,
        };

        if (this.statusCode === '0000') {
            result.data = this.data
        }

        return res.status(200).json(result);
    }

}

module.exports = Util
