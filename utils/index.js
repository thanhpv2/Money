function convertPhoneTo0x(str) {
  if (!str)
    return false
  if (typeof str !== 'string' && typeof str !== 'number')
    return false
  const phone = str.trim()
  if (phone.indexOf('84') === 0) {
    return phone.replace('84', 0)
  }
  if (phone.indexOf('0') !== 0) {
    return `0${phone}`
  }
  return phone
}

module.exports = {
  convertPhoneTo0x,
}