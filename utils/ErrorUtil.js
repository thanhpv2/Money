const errorCode = {
  '000': 'Thành công',
  300: 'Lỗi không xác định',
  400: 'Dữ liệu không hợp lệ',
  401: 'Thiếu dữ liệu đầu vào',
  402: 'Dữ liệu không đúng định dạng',
  403: 'Dữ liệu không trùng khớp',
  404: 'Dữ liệu vượt quá giới hạn cho phép',
  500: 'Tham số không hợp lệ',
  501: 'Thiếu tham số đầu vào',
}

function getErrorMessage(code) {
  return errorCode[code] || 'Lỗi không xác định'
}

function formatErrorMessage(error) {
  return error.message || error
}

module.exports = {
  getErrorMessage,
  formatErrorMessage,
}