const dotenv = require('dotenv')
dotenv.config({ path: '.env' })

module.exports = {
  development: {
    username: process.env.POSTGRE_USER,
    password: process.env.POSTGRE_PASSWORD,
    database: process.env.POSTGRE_DATABASE,
    host: process.env.POSTGRE_HOST,
    port: process.env.POSTGRE_PORT,
    dialect: 'postgres',
    logging: false
  },
  production: {
    username: process.env.POSTGRE_USER,
    password: process.env.POSTGRE_PASSWORD,
    database: process.env.POSTGRE_DATABASE,
    host: process.env.POSTGRE_HOST,
    port: process.env.POSTGRE_PORT,
    dialect: 'postgres'
  }
}