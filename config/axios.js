'use strict'

const axios = require('axios')

function setupKioAxios(token) {
    console.log(token);
    axios.interceptors.request.use(
        config => {
            config.headers.Authorization = `Bearer ${token}`;
            config.headers.Retailer = process.env.KIOT_RETAIL;
            if (config.url && config.url.indexOf('http') < 0) {
                config.url = `https://public.kiotapi.com/${config.url}`
            }
            return config;
        },
        err => Promise.reject(err)
    );
}

function setupAxios(params) {
    
}

module.exports = {
    setupKioAxios,
    setupAxios,
}
