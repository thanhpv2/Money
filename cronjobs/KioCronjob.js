'use strict'

var CronJob = require('cron').CronJob;
var axios = require('axios');

class KioCronjob {

    constructor() {
        this.token
        this.retry
        this.run = this.run.bind(this)
        this.fetchToken = this.fetchToken.bind(this)
    }

    run() {
        var self = this
        var job = new CronJob('00 00 * * * *', function () {
            self.fetchToken()
        }, null, true, 'Asia/Ho_Chi_Minh');
        job.start();
    }

    fetchToken() {
        const params = new URLSearchParams()
        params.append('scopes', 'PublicApi.Access')
        params.append('grant_type', 'client_credentials')
        params.append('client_id', process.env.KIOT_CLIENT_ID)
        params.append('client_secret', process.env.KIOT_CLIENT_SECRET)

        axios.post(`https://id.kiotviet.vn/connect/token`, params, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    this.token = res.data.access_token
                }
            })
            .catch(err => console.log(err))
    }

    setToken() {
        this.fetchToken()
    }

    getToken() {
        return this.token
    }
}

module.exports = KioCronjob
