const { isPhone } = require(".")

function validCustomerCreateDelivery(data) {
  if (!data.memberId || !data.fullName || !data.phone || !data.provinceId || !data.districtId || !data.address) {
    return 401
  }
  if (!isPhone(data.phone)) {
    return 400
  }
}

function validCustomerUpdateDelivery(data) {
  console.log(data);
  if (!Object.keys(data).length) {
    return 401
  }
  if ((typeof data.fullName !== 'undefined' && !data.fullName)
    || (typeof data.phone !== 'undefined' && !data.phone)
    || (typeof data.provinceId !== 'undefined' && !data.provinceId)
    || (typeof data.districtId !== 'undefined' && !data.districtId)
    || (typeof data.address !== 'undefined' && !data.address)
    || (data.phone && !isPhone(data.phone))
  ) {
    return 400
  }
  return false
}

module.exports = {
  validCustomerCreateDelivery,
  validCustomerUpdateDelivery,
}
