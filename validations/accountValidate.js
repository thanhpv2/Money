const Validation = require('./index')

function validChangePassword(data) {
  if (!data.password || !data.rePassword || !data.oldPassword) {
    return 401
  }
  if (!Validation.isLength(data.password, { min: 8 })) {
    return 400
  }
  if (data.password !== data.rePassword) {
    return 403
  }
}

module.exports = {
  validChangePassword
}
