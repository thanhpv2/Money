const Validation = require('./index')

function validCustomerCreateOrder(data) {
  if (!data.customerId || !data.products || (Array.isArray(data.products) && !data.products.length) || !data.paymentMethod || !data.totalAmount || !data.deliveryId) {
    return 401
  }
  if (!Array.isArray(data.products) || !Validation.isUUID(data.customerId) || !['cash', 'bank', 'card'].includes(data.paymentMethod) || !Number(data.totalAmount) || !Validation.isUUID(data.deliveryId)) {
    return 400
  }
  if ((data.customerNote && data.customerNote.length > 1000)) {
    return 404
  }
  const productInvalid = data.products.find(product => !Validation.isUUID(product.id) || !Number.isInteger(product.price) || !product.price || !Number.isInteger(product.quantity) || !product.quantity)
  if (productInvalid) {
    return 400
  }
}

module.exports = {
  validCustomerCreateOrder
}
