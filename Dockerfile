FROM node:12-slim

WORKDIR /app

COPY package.json /tmp/package.json
RUN cd /tmp && npm install --production
RUN mkdir -p /app && cp -a /tmp/node_modules /app/

RUN ls -l

COPY . /app

CMD ["npm","start"]

EXPOSE 3001
