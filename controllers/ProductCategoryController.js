'use strict'

const { beautifyJson } = require('../helpers')
const ProductCategoryService = require(`../services/ProductCategoryService`)

async function getItems(req, res) {
    try {
        const query = req.query
        const { rows, count } = await ProductCategoryService.getAll(query)
        return res.send({ data: rows, total: count })
    } catch (error) {
        console.log(error);
    }
}

async function getItem(req, res) {
    try {
        const { id } = req.params
        const category = await ProductCategoryService.getById(id)
        return res.success({ data: category })
    } catch (error) {
        console.log(error);
        return res.error({ error })
    }
}


async function updateItem(req, res) {
    try {
        const { id } = req.params
    } catch (error) {

    }
}


async function createItem(req, res) {
    try {
        const data = beautifyJson(req.body)
    } catch (error) {

    }
}


async function deleteItem(req, res) {
    try {
        const { id } = req.params
    } catch (error) {

    }
}

module.exports = {
    getItems,
    getItem,
    updateItem,
    createItem,
    deleteItem,
}