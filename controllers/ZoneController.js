'use strict'

const { buildQuery } = require('../helpers/queryHelper')
const ZoneService = require(`../services/ZoneService`)

async function getLevel1Items(req, res) {
  try {
    const query = buildQuery(req.query)
    const { rows, count } = await ZoneService.getProvinces(query)
    return res.success({ data: rows, total: count })
  } catch (error) {
    console.log(error);
    return res.error({ message: error })
  }
}

async function getLevel2Items(req, res) {
  try {
    const query = buildQuery(req.query)
    query.where.parentId = req.query.provinceId
    const { rows, count } = await ZoneService.getDistrictByProvinceId(query)
    return res.success({ data: rows, total: count })
  } catch (error) {
    console.log(error);
    return res.error({ message: error })
  }
}

async function getItem(req, res) {
  try {
    const { id } = req.params
    const item = await ZoneService.getById(id)
    if (item) {
      return res.success({ data: item })
    }
    return res.error({ message: 'not found', status: 404 })
  } catch (error) {
    console.log(error);
    return res.error({ message: error })
  }
}

module.exports = {
  getLevel1Items,
  getLevel2Items,
  getItem,
}
