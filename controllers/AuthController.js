const jwt = require('jsonwebtoken')
const { beautifyJson } = require('../helpers')
const MemberService = require('../services/MemberService')
const AuthService = require('../services/AuthService')

async function login(req, res) {
  try {
    const body = beautifyJson(req.body)
    const { username, password } = body
    let user = await MemberService.getOne({ username })
    if (!user) {
      return res.status(401).send({ message: 'Tài khoản không tồn tại' })
    }
    if (!await user.validPassword(password)) {
      throw new Error('Mật khẩu không chính xác')
    }
    if (!user.isActive) {
      throw new Error('Tài khoản đang bị khóa')
    }
    const data = {
      sub: user.id,
      username: user.username,
      email: user.email,
      phone: user.phone,
      fullName: user.fullName,
    }
    const result = AuthService.encodeData(data)
    return res.status(200).send(result)
  } catch (error) {
    return res.status(400).send({ message: error.message })
  }
}

async function refreshToken(req, res) {
  try {
    const { refreshToken } = req.body
    var decode = jwt.verify(refreshToken, process.env.JWT_REFRESH_TOKEN_SECRET);
    const data = {
      sub: decode.sub,
      username: decode.username,
      email: decode.email,
      phone: decode.phone,
      fullName: decode.fullName,
    }
    const result = AuthService.encodeData(data)
    return res.status(200).send(result)
  } catch (error) {
    console.log(error);
    return res.status(400).send({ message: error.message })
  }
}

async function logout(req, res) {
  try {
    req.logout();
    return res.send({ authenticated: req.isAuthenticated() })
  } catch (error) {
    return res.status(400).send({ message: error.message })
  }
}

async function register(req, res) {
  try {
    const data = beautifyJson(req.body)
    let user = await MemberService.create(data)
    if (!user) {
      throw new Error('Có lỗi xảy ra. Vui lòng thử lại sau')
    }
    return res.status(201).send({ message: 'Thành công' })
  } catch (error) {
    return res.status(400).send({ message: error.message })
  }
}

async function getReferralCode(req, res) {
  try {
    const { phoneNumber } = req.params
    if (phoneNumber) {
      const user = await MemberService.findByPhone(phoneNumber)
      if (user && user.isActive) {
        return res.status(200).send(user.referralCode)
      }
    }
    return res.status(404).send('')
  } catch (error) {
    return res.status(400).send({ message: error.message })
  }
}

module.exports = {
  login,
  logout,
  register,
  getReferralCode,
  refreshToken,
}