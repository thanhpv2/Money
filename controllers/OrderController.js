'use strict'

const { beautifyJson } = require('../helpers')
// const KioOrderService = require('../KioServices/OrderService')
const OrderService = require(`../services/OrderService`)
const OrderValidation = require('../validations/orderValidate')
// const { getErrorMessage } = require('../utils/ErrorUtil')

async function getItems(req, res) {
  try {
    const query = beautifyJson(req.query)
    const { rows, count } = await OrderService.getAll(query)
    return res.send({ data: rows, total: count })
  } catch (error) {
    console.log(error);
    return res.status(400).send({ message: error.message })
  }
}

async function getItem(req, res) {
  try {
    const { id } = req.params
    const order = await OrderService.getById(id)
    return res.send(order)
  } catch (error) {
    console.log(error);
    return res.status(400).send({ message: error.message })
  }
}

async function updateItem(req, res) {
  try {
    const { id } = req.params
    const data = req.body
    const order = await OrderService.getById(id)
    if (!order) {
      throw new Error('Đơn hàng không tồn tại')
    }
    await OrderService.update(id, data)
    return res.send(order)
  } catch (error) {
    return res.status(400).send({ message: error.message })
  }
}

async function createItem(req, res) {
  try {
    const data = beautifyJson(req.body)
  } catch (error) {
    return res.status(400).send({ message: error.message })
  }
}

async function deleteItem(req, res) {
  try {
    const { id } = req.params
  } catch (error) {
    return res.status(400).send({ message: error.message })
  }
}

async function createFromCustomer(req, res) {
  try {
    const data = beautifyJson(req.body)
    data.customerId = req.jwtData.sub
    const error = OrderValidation.validCustomerCreateOrder(data)
    if (error) {
      return res.error({ code: error })
    }
    const created = await OrderService.create(data)
    if (!created) {
      throw new Error('Có lỗi xảy ra. Vui lòng thử lại sau')
    }
    // Bắn đơn sang KiotViet
    // KioOrderService.create(created)
    return res.status(200).send({ message: 'Thành công', created })
  } catch (error) {
    return res.error({ message: error.message })
  }
}

module.exports = {
  getItems,
  getItem,
  updateItem,
  createItem,
  deleteItem,
  createFromCustomer,
}