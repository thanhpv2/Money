'use strict'

const MigrationService = require(`../services/MigrationService`)

async function migrateKioProducts(req, res) {
  try {
    const { limit, page } = req.query
    MigrationService.fetchKiotProducts(limit, page)
    return res.send('Success')
  } catch (error) {
    console.log(error);
    return res.status(400).send('Error')
  }
}

async function migrateKioCategories(req, res) {
  try {
    const { limit, page } = req.query
    MigrationService.fetchKiotCategories(limit, page)
    return res.send('Success')
  } catch (error) {
    console.log(error);
    return res.status(400).send('Error')
  }
}

async function migrateKioCustomers(req, res) {
  try {
    const { limit, page } = req.query
    MigrationService.fetchKiotCustomers(limit, page)
    return res.send('Success')
  } catch (error) {
    console.log(error);
    return res.status(400).send('Error')
  }
}

async function migrateKioOrders(req, res) {
  try {
    const { limit, page } = req.query
    MigrationService.fetchKiotOrders(limit, page)
    return res.send('Success')
  } catch (error) {
    console.log(error);
    return res.status(400).send('Error')
  }
}

module.exports = {
  migrateKioProducts,
  migrateKioCategories,
  migrateKioCustomers,
  migrateKioOrders,
}