'use strict'

const { beautifyJson } = require('../helpers')
const { buildQuery } = require('../helpers/queryHelper')
const { getErrorMessage } = require('../utils/ErrorUtil')
const DeliveryService = require(`../services/DeliveryService`)
const DeliveryValidation = require('../validations/deliveryValidate')

async function getItems(req, res) {
  try {
    const query = buildQuery(req.query)
    if (req.query.memberId) {
      query.where.memberId = req.query.memberId
    }
    const { rows, count } = await DeliveryService.getAll(query)
    return res.success({ data: rows, total: count })
  } catch (error) {
    console.log(error);
    return res.status(400).send({ message: error })
  }
}

async function getItem(req, res) {
  try {
    const { id } = req.params
    const item = await DeliveryService.getById(id)
    if (item) {
      return res.success({ data: item })
    }
    return res.error({ message: 'not found', status: 404 })
  } catch (error) {
    console.log(error);
    return res.error({ error })
  }
}

async function updateItem(req, res) {
  try {
    const { id } = req.params
    const query = { id }
    if (req.query.memberId) {
      query.memberId = req.query.memberId
    }
    const data = beautifyJson(req.body)
    const error = DeliveryValidation.validCustomerUpdateDelivery(data)
    const item = await DeliveryService.getOne(query)
    if (!item) {
      return res.error({ message: 'not found' })
    }
    if (error) {
      throw new Error(getErrorMessage(error))
    }
    await DeliveryService.update(id, data)
    return res.success({ status: 204 })
  } catch (error) {
    return res.status(400).send({ message: error.message || error })
  }
}

async function createItem(req, res) {
  try {
    const data = beautifyJson(req.body)
    if (req.query.memberId) {
      const max = await DeliveryService.countByMemberId(req.query.memberId)
      if (max === 5) {
        throw new Error('reach limited')
      }
      else if (max === 0) {
        data.isDefault = true
      }
      data.memberId = req.query.memberId
    }
    const error = await DeliveryValidation.validCustomerCreateDelivery(data)
    if (error) {
      throw new Error(getErrorMessage(error))
    }
    const item = await DeliveryService.create(data)
    if (item) {
      return res.success({ message: 'success', data: item })
    }
    return res.error({ message: 'failed' })
  } catch (error) {
    return res.error({ message: error })
  }
}

async function deleteItem(req, res) {
  try {
    const { id } = req.params
  } catch (error) {

  }
}

module.exports = {
  getItems,
  getItem,
  updateItem,
  createItem,
  deleteItem,
}
