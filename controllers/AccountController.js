const { beautifyJson } = require('../helpers')
const MemberService = require('../services/MemberService')
const ErrorUtil = require('../utils/ErrorUtil')
const { validChangePassword } = require('../validations/accountValidate')

async function changePassword(req, res) {
  try {
    const jwtData = req.jwtData
    const data = beautifyJson(req.body)
    const { oldPassword, password } = data
    const error = validChangePassword(data)
    if (error) {
      throw new Error(ErrorUtil.getErrorMessage(error))
    }
    const user = await MemberService.getById(jwtData.sub)
    if (!user) {
      return res.status(401).send({ message: 'Tài khoản không tồn tại' })
    }
    if (!await user.validPassword(oldPassword)) {
      throw new Error('Mật khẩu cũ không chính xác')
    }
    user.passwordHash = await user.generateHash(password)
    await user.save()
    return res.status(204).send({ message: 'Thành công' })
  } catch (error) {
    console.log(error);
    return res.status(400).send({ message: error.message })
  }
}

module.exports = {
  changePassword,
}