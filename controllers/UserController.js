'use strict'

const UserService = require('../services/UserService')

async function getItems(req, res) {
    try {
        const query = req.query
        const users = await UserService.getAll(query)
        return req.send(user)
    } catch (error) {

    }
}


async function getItem(req, res) {
    try {
        const { id } = req.params
        const user = await UserService.getOne({ id })
        return req.send(user)
    } catch (error) {

    }
}


async function updateItem(req, res) {
    try {
        const { id } = req.params
    } catch (error) {

    }
}


async function createItem(req, res) {
    try {
        const data = formatBody(req.body)
    } catch (error) {

    }
}


async function deleteItem(req, res) {
    try {
        const { id } = req.params
    } catch (error) {

    }
}

module.exports = {
    getItems,
    getItem,
    updateItem,
    createItem,
    deleteItem,
}