'use strict'

const { beautifyJson } = require('../helpers')
const { buildQuery } = require('../helpers/queryHelper')
const ProductService = require(`../services/ProductService`)

async function getItems(req, res) {
    try {
        const query = buildQuery(req.query)
        const { rows, count } = await ProductService.getAll(query)
        return res.send({ data: rows, total: count })
    } catch (error) {
        console.log(error);
        return res.status(400).send({ message: error.message })
    }
}

async function getItem(req, res) {
    try {
        const { id } = req.params
        const product = await ProductService.getById(id)
        return res.send(product)
    } catch (error) {
        console.log(error);
    }
}


async function updateItem(req, res) {
    try {
        const { id } = req.params
    } catch (error) {

    }
}


async function createItem(req, res) {
    try {
        const data = beautifyJson(req.body)
    } catch (error) {

    }
}


async function deleteItem(req, res) {
    try {
        const { id } = req.params
    } catch (error) {

    }
}

async function getInventoryItems(req, res) {
    try {
        const query = buildQuery(req.query)
        const { rows, count } = await ProductService.getAllByQuantity(query)
        return res.send({ data: rows, total: count })
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}

async function getSaleProducts(req, res) {
    try {
        const query = buildQuery(req.query)
        const { rows, count } = await ProductService.getAllBySale(query)
        return res.send({ data: rows, total: count })
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}

module.exports = {
    getItems,
    getItem,
    updateItem,
    createItem,
    deleteItem,
    getInventoryItems,
    getSaleProducts,
}