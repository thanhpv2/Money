const jwt = require('jsonwebtoken');

module.exports = function () {
  return (req, res, next) => {
    try {
      let token = req.headers.authorization || ''
      token = token.replace('Bearer ', '')

      const decoded = jwt.verify(token, process.env.JWT_TOKEN_SECRET);
      if (decoded) {
        req.jwtData = decoded
        req.query.memberId = decoded.sub
        return next();
      }
      throw new Error('error')
    }
    catch (error) {
      console.log(error);
      return res.status(403).json({ message: "Yêu cầu bị từ chối" });
    }
  }
}
