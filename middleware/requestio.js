
const { formatErrorMessage, getErrorMessage } = require('../utils/ErrorUtil')

module.exports = function (req, res, next) {
  res.success = function ({ data, message = 'success', status = 200, ...options }) {
    return res.status(status).send({
      code: '000',
      data,
      message,
      ...options
    })
  }
  res.error = function ({ status = 400, code = 300, message = '' }) {
    return res.status(status).send({ code, message: message ? formatErrorMessage(message) : getErrorMessage(code) })
  }
  next()
}
