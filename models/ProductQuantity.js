'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductQuantity extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Product, {
        foreignKey: 'productId',
        targetKey: 'id'
      })
    }
  };
  ProductQuantity.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    productId: {
      type: DataTypes.UUID,
      unique: true,
    },
    quantity: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    createdBy: DataTypes.UUID,
    updatedBy: DataTypes.UUID,
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'ProductQuantity',
    tableName: 'product_quantity'
  }, {
    indexes: [
      {
        unique: true,
        fields: ['productId']
      },
    ]
  });
  return ProductQuantity;
};