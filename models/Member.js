'use strict';
const { Model } = require('sequelize');
const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  class Member extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Order, {
        sourceKey: 'id',
        foreignKey: 'customerId'
      })
    }
  };
  Member.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    customerId: DataTypes.INTEGER,
    username: DataTypes.STRING,
    passwordHash: DataTypes.STRING,
    phone: [DataTypes.STRING],
    email: DataTypes.STRING,
    birthday: DataTypes.DATE,
    fullName: DataTypes.STRING,
    rankId: DataTypes.UUID,
    avatar: DataTypes.STRING,
    provinceId: DataTypes.UUID,
    districtId: DataTypes.UUID,
    address: DataTypes.STRING,
    referralCode: DataTypes.STRING,
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdBy: DataTypes.UUID,
    updatedBy: DataTypes.UUID,
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Member',
    tableName: 'member',
    freezeTableName: true,
  });

  Member.beforeCreate((user, options) => {
    return user.generateHash(user.passwordHash)
      .then(hash => {
        user.passwordHash = hash;
      })
      .catch(err => {
        throw new Error();
      });
  });

  Member.prototype.generateHash = function (pwd) {
    return bcrypt.hash(pwd, bcrypt.genSaltSync(10));
  }

  Member.prototype.validPassword = function (pwd) {
    // console.log(pwd, this.passwordHash);
    return bcrypt.compare(pwd, this.passwordHash);
  }

  return Member;
};