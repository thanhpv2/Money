'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductProductAttributeValue extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  ProductProductAttributeValue.init({
    productProductAttributeId: DataTypes.UUID,
    value: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ProductProductAttributeValue',
  });
  return ProductProductAttributeValue;
};