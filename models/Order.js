'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.OrderProduct, {
        sourceKey: 'id',
        foreignKey: 'orderId'
      })
      this.belongsTo(models.Member, {
        foreignKey: 'customerId',
        targetKey: 'id'
      })
    }
  };
  Order.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    code: {
      type: DataTypes.STRING,
    },
    orderId: {
      type: DataTypes.INTEGER,
      comment: 'ID đơn hàng Kiot'
    },
    customerId: {
      type: DataTypes.UUID
    },
    deliveryId: {
      type: DataTypes.UUID,
      comment: 'ID địa chỉ giao hàng',
      allowNull: true
    },
    countProduct: {
      type: DataTypes.INTEGER
    },
    discount: {
      type: DataTypes.FLOAT,
      comment: 'Giảm giá: 0-100 (%), > 100 (vnd)'
    },
    totalAmount: {
      type: DataTypes.FLOAT
    },
    paymentMethod: {
      type: DataTypes.ENUM('cash', 'bank', 'card')
    },
    isPaid: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      comment: 'Khách hàng đã thanh toán'
    },
    customerNote: {
      type: DataTypes.STRING(1000),
      comment: 'Khách hàng ghi chú'
    },
    userNote: {
      type: DataTypes.STRING(1000),
      comment: 'Nhân viên ghi chú'
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isCompleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      comment: 'Đơn hàng đã hoàn thành'
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdBy: {
      type: DataTypes.UUID
    },
    updatedBy: {
      type: DataTypes.UUID
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'Order',
    tableName: 'order'
  });
  return Order;
};