'use strict';

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Order, {
        foreignKey: 'orderId',
        targetKey: 'id'
      })
      this.hasOne(models.Product, {
        sourceKey: 'productId',
        foreignKey: 'id'
      })
    }
  };
  OrderProduct.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    orderId: {
      type: DataTypes.UUID
    },
    productId: {
      type: DataTypes.UUID
    },
    basePrice: {
      type: DataTypes.FLOAT
    },
    price: {
      type: DataTypes.FLOAT
    },
    discount: {
      type: DataTypes.FLOAT
    },
    quantity: {
      type: DataTypes.INTEGER
    },
    amount: {
      type: DataTypes.FLOAT
    },
  }, {
    sequelize,
    modelName: 'OrderProduct',
    tableName: 'order_product',
    timestamps: false
  });
  return OrderProduct;
};