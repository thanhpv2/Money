'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductProductCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  ProductProductCategory.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    productId: DataTypes.UUID,
    productCategoryId: DataTypes.UUID,
  }, {
    sequelize,
    modelName: 'ProductProductCategory',
    tableName: 'product_product_category',
    timestamps: false,
    freezeTableName: true,
  });
  return ProductProductCategory;
};