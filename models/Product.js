'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.ProductQuantity, {
        sourceKey: 'id',
        foreignKey: 'productId'
      })
      this.hasMany(models.ProductProductCategory, {
        sourceKey: 'id',
        foreignKey: 'productId'
      })
    }
  };
  Product.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    productId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    description: DataTypes.STRING,
    basePrice: DataTypes.FLOAT,
    salePrice: DataTypes.FLOAT,
    saleValue: DataTypes.FLOAT,
    images: [DataTypes.STRING],
    allowSale: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    featured: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdBy: DataTypes.UUID,
    updatedBy: DataTypes.UUID,
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Product',
    tableName: 'product'
  });
  return Product;
};