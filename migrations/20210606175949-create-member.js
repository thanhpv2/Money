'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('member', {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      customerId: Sequelize.INTEGER,
      username: {
        type: Sequelize.STRING
      },
      passwordHash: {
        type: Sequelize.STRING
      },
      fullName: Sequelize.STRING,
      phone: Sequelize.ARRAY(Sequelize.STRING),
      email: Sequelize.STRING,
      birthday: Sequelize.DATE,
      rankId: {
        type: Sequelize.STRING
      },
      avatar: {
        type: Sequelize.STRING
      },
      provinceId: {
        type: Sequelize.UUID
      },
      districtId: {
        type: Sequelize.UUID
      },
      address: Sequelize.STRING,
      referralCode: Sequelize.STRING,
      isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdBy: Sequelize.UUID,
      updatedBy: Sequelize.UUID,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('member');
  }
};