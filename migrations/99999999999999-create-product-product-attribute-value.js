'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('product_product_attribute_value', {
      productProductAttributeId: Sequelize.UUID,
      value: Sequelize.STRING
    })
      .then(() => queryInterface.addIndex('product_product_attribute_value', ['productProductAttributeId']));
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('product_product_attribute_value');
  }
};