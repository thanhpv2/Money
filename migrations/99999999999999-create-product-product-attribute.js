'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('product_product_attribute', {
      productId: Sequelize.UUID,
      productAttributeId: Sequelize.UUID,
    })
      .then(() => queryInterface.addIndex('product_product_attribute', ['productId', 'productAttributeId']));
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('product_product_attribute');
  }
};