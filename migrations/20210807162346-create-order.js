'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('order', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
      },
      code: {
        type: Sequelize.STRING
      },
      orderId: {
        type: Sequelize.INTEGER
      },
      customerId: {
        type: Sequelize.UUID
      },
      deliveryId: {
        type: Sequelize.UUID,
        comment: 'ID địa chỉ giao hàng',
      },
      countProduct: {
        type: Sequelize.INTEGER
      },
      discount: {
        type: Sequelize.FLOAT,
        comment: 'Giảm giá: 0-100 (%), > 100 (vnd)'
      },
      totalAmount: {
        type: Sequelize.FLOAT
      },
      paymentMethod: {
        type: Sequelize.ENUM('cash', 'bank', 'card')
      },
      isPaid: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: 'Khách hàng đã thanh toán'
      },
      customerNote: {
        type: Sequelize.STRING(1000),
        comment: 'Khách hàng ghi chú'
      },
      userNote: {
        type: Sequelize.STRING(1000),
        comment: 'Nhân viên ghi chú'
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      isCompleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: 'Đơn hàng đã hoàn thành'
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdBy: {
        type: Sequelize.UUID
      },
      updatedBy: {
        type: Sequelize.UUID
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('order');
  }
};