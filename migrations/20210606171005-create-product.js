'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('product', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
      },
      productId: {
        type: Sequelize.INTEGER,
      },
      name: Sequelize.STRING,
      code: Sequelize.STRING,
      description: Sequelize.STRING,
      basePrice: Sequelize.FLOAT,
      salePrice: Sequelize.FLOAT,
      saleValue: Sequelize.FLOAT,
      images: Sequelize.ARRAY(Sequelize.STRING),
      allowSale: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      featured: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdBy: Sequelize.UUID,
      updatedBy: Sequelize.UUID,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('product');
  }
};