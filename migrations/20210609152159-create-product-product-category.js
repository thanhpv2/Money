'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('product_product_category', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
      },
      productId: {
        type: Sequelize.UUID
      },
      productCategoryId: {
        type: Sequelize.UUID
      }
    })
      .then(() => queryInterface.addIndex('product_product_category', ['productId', 'productCategoryId']));
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('product_product_category');
  }
};