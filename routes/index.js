const express = require('express')
const Router = express.Router()
const adminRoutes = require('./admin')
const frontendRoutes = require('./frontend')
const masterDataRoutes = require('./master_data')

Router.use('/admin', adminRoutes)
Router.use('/api', frontendRoutes)
Router.use('/api/master-data', masterDataRoutes)

module.exports = Router