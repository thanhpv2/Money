'use strict'

const Router = require('express').Router()
const CategoryPostController = require('../controllers/CategoryPostController')

Router.get('/', CategoryPostController.getItems)
Router.get('/:id', CategoryPostController.getItem)
Router.put('/:id', CategoryPostController.updateItem)
Router.post('/', CategoryPostController.createItem)
Router.delete('/:id', CategoryPostController.deleteItem)

module.exports = Router