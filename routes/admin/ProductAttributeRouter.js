'use strict'

const Router = require('express').Router()
const ProductAttributeController = require('../controllers/ProductAttributeController')

Router.get('/', ProductAttributeController.getItems)
Router.get('/:id', ProductAttributeController.getItem)
Router.put('/:id', ProductAttributeController.updateItem)
Router.post('/', ProductAttributeController.createItem)
Router.delete('/:id', ProductAttributeController.deleteItem)

module.exports = Router