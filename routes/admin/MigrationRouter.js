'use strict'

const Router = require('express').Router()
const MigrationController = require('../../controllers/MigrationController')

Router.post('/products', MigrationController.migrateKioProducts)
Router.post('/categories', MigrationController.migrateKioCategories)
Router.post('/members', MigrationController.migrateKioCustomers)
Router.post('/orders', MigrationController.migrateKioOrders)

module.exports = Router