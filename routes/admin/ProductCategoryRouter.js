'use strict'

const Router = require('express').Router()
const ProductCategoryController = require('../../controllers/ProductCategoryController')

Router.get('/', ProductCategoryController.getItems)
Router.get('/:id', ProductCategoryController.getItem)
Router.put('/:id', ProductCategoryController.updateItem)
Router.post('/', ProductCategoryController.createItem)
Router.delete('/:id', ProductCategoryController.deleteItem)

module.exports = Router