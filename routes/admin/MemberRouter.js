'use strict'

const Router = require('express').Router()
const MemberController = require('../../controllers/MemberController')

Router.get('/', MemberController.getItems)
Router.get('/:id', MemberController.getItem)
Router.put('/:id', MemberController.updateItem)
Router.post('/', MemberController.createItem)
Router.delete('/:id', MemberController.deleteItem)

module.exports = Router