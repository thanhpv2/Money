'use strict'

const Router = require('express').Router()
const OrderController = require('../../controllers/OrderController')

Router.get('/', OrderController.getItems)
Router.get('/:id', OrderController.getItem)
Router.put('/:id', OrderController.updateItem)
Router.post('/', OrderController.createItem)
Router.delete('/:id', OrderController.deleteItem)

module.exports = Router