'use strict'

const Router = require('express').Router()
const TransactionController = require('../controllers/TransactionController')

Router.get('/', TransactionController.getItems)
Router.get('/:id', TransactionController.getItem)
Router.put('/:id', TransactionController.updateItem)
Router.post('/', TransactionController.createItem)
Router.delete('/:id', TransactionController.deleteItem)

module.exports = Router