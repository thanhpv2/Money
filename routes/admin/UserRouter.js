'use strict'

const Router = require('express').Router()
const UserController = require('../controllers/UserController')

Router.get('/', UserController.getItems)
Router.get('/:id', UserController.getItem)
Router.put('/:id', UserController.updateItem)
Router.post('/', UserController.createItem)
Router.delete('/:id', UserController.deleteItem)

module.exports = Router