'use strict'

const Router = require('express').Router()
const PostController = require('../controllers/PostController')

Router.get('/', PostController.getItems)
Router.get('/:id', PostController.getItem)
Router.put('/:id', PostController.updateItem)
Router.post('/', PostController.createItem)
Router.delete('/:id', PostController.deleteItem)

module.exports = Router