'use strict'

const Router = require('express').Router()
const ProductController = require('../../controllers/ProductController')

Router.get('/', ProductController.getItems)
Router.get('/:id', ProductController.getItem)
Router.put('/:id', ProductController.updateItem)
Router.post('/', ProductController.createItem)
Router.delete('/:id', ProductController.deleteItem)

module.exports = Router