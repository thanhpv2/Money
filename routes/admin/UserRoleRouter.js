'use strict'

const Router = require('express').Router()
const UserRoleController = require('../controllers/UserRoleController')

Router.get('/', UserRoleController.getItems)
Router.get('/:id', UserRoleController.getItem)
Router.put('/:id', UserRoleController.updateItem)
Router.post('/', UserRoleController.createItem)
Router.delete('/:id', UserRoleController.deleteItem)

module.exports = Router