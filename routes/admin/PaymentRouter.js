'use strict'

const Router = require('express').Router()
const PaymentController = require('../controllers/PaymentController')

Router.get('/', PaymentController.getItems)
Router.get('/:id', PaymentController.getItem)
Router.put('/:id', PaymentController.updateItem)
Router.post('/', PaymentController.createItem)
Router.delete('/:id', PaymentController.deleteItem)

module.exports = Router