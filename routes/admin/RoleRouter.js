'use strict'

const Router = require('express').Router()
const RoleController = require('../controllers/RoleController')

Router.get('/', RoleController.getItems)
Router.get('/:id', RoleController.getItem)
Router.put('/:id', RoleController.updateItem)
Router.post('/', RoleController.createItem)
Router.delete('/:id', RoleController.deleteItem)

module.exports = Router