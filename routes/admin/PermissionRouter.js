'use strict'

const Router = require('express').Router()
const PermissionController = require('../controllers/PermissionController')

Router.get('/', PermissionController.getItems)
Router.get('/:id', PermissionController.getItem)
Router.put('/:id', PermissionController.updateItem)
Router.post('/', PermissionController.createItem)
Router.delete('/:id', PermissionController.deleteItem)

module.exports = Router