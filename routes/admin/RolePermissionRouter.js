'use strict'

const Router = require('express').Router()
const RolePermissionController = require('../controllers/RolePermissionController')

Router.get('/', RolePermissionController.getItems)
Router.get('/:id', RolePermissionController.getItem)
Router.put('/:id', RolePermissionController.updateItem)
Router.post('/', RolePermissionController.createItem)
Router.delete('/:id', RolePermissionController.deleteItem)

module.exports = Router