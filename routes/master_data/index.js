const express = require('express')
const Router = express.Router()

const DistrictRoute = require('./DistrictRoute')
const ProvinceRoute = require('./ProvinceRoute')

Router.use('/provinces', ProvinceRoute)
Router.use('/districts', DistrictRoute)

module.exports = Router
