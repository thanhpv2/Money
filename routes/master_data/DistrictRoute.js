'use strict'

const Router = require('express').Router()
const ZoneController = require('../../controllers/ZoneController')

Router.get('/', ZoneController.getLevel2Items)
Router.get('/:id', ZoneController.getItem)

module.exports = Router