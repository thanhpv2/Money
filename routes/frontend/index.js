const express = require('express')
const Router = express.Router()

const authenticate = require('../../middleware/authenticate')

const AuthRouter = require('./AuthRouter')
const AccountRouter = require('./AccountRouter')
const DeliveryRouter = require('./DeliveryRouter')
const CheckoutRouter = require('./CheckoutRouter')
const ProductRouter = require('./ProductRouter')
const ProductCategoryRouter = require('./ProductCategoryRouter')

Router.use('/auth', AuthRouter)
Router.use('/accounts', authenticate(), AccountRouter)
Router.use('/deliveries', authenticate(), DeliveryRouter)
Router.use('/checkouts', authenticate(), CheckoutRouter)
Router.use('/products', ProductRouter)
Router.use('/product-categories', ProductCategoryRouter)

module.exports = Router
