'use strict'

const Router = require('express').Router()
const ProductController = require('../../controllers/ProductController')

Router.get('/', ProductController.getItems)
Router.get('/inventory', ProductController.getInventoryItems)
Router.get('/sales', ProductController.getSaleProducts)
Router.get('/:id', ProductController.getItem)

module.exports = Router