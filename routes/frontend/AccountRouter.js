'use strict'

const Router = require('express').Router()
const AccountController = require('../../controllers/AccountController')

Router.post('/change-password', AccountController.changePassword)

module.exports = Router