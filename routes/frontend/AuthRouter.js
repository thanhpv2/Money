'use strict'

const Router = require('express').Router()
const AuthController = require('../../controllers/AuthController')

Router.post('/login', AuthController.login)
Router.post('/logout', AuthController.logout)
Router.post('/register', AuthController.register)
Router.post('/refresh-token', AuthController.refreshToken)

module.exports = Router