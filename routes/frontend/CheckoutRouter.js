'use strict'

const Router = require('express').Router()
const OrderController = require('../../controllers/OrderController')

Router.post('/', OrderController.createFromCustomer)

module.exports = Router