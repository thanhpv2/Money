'use strict'

const Router = require('express').Router()
const DeliveryController = require('../../controllers/DeliveryController')

Router.get('/', DeliveryController.getItems)
Router.get('/:id', DeliveryController.getItem)
Router.post('/', DeliveryController.createItem)
Router.put('/:id', DeliveryController.updateItem)
Router.delete('/:id', DeliveryController.deleteItem)

module.exports = Router