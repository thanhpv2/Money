'use strict'

const Router = require('express').Router()
const ProductCategoryController = require('../../controllers/ProductCategoryController')

Router.get('/', ProductCategoryController.getItems)
Router.get('/:id', ProductCategoryController.getItem)

module.exports = Router