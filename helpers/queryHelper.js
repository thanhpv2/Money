'use strict'

const Op = require('sequelize').Op

function buildKioQuery(params = {}) {
  const query = { ...params }
  if (Number(params.limit)) {
    query.pageSize = Number(params.limit)
  }
  if (Number(params.page)) {
    query.currentItem = query.limit * (Number(params.page) - 1)
  }
  query.includeInventory = true
  return query
}

function buildQuery(params = {}) {
  const query = {
    params,
    where: {
      isDeleted: false,
    },
    limit: 10,
    offset: 0,
    order: [['updatedAt', 'DESC']]
  }
  if (Number(params.limit)) {
    query.limit = Number(params.limit)
  }
  if (Number(params.page)) {
    query.offset = query.limit * (Number(params.page) - 1)
  }
  if (params.name) {
    query.where.name = {
      [Op.iLike]: `%${params.name}%`
    }
  }
  if (params.code) {
    query.where.code = {
      [Op.iLike]: `%${params.code}%`
    }
  }
  if (params.keyword) {
    query.where = {
      ...query.where,
      [Op.or]: [
        {
          name: {
            [Op.iLike]: `%${params.keyword}%`
          }
        },
        {
          code: {
            [Op.iLike]: `%${params.keyword}%`
          }
        }
      ]
    }
  }
  return query
}

module.exports = {
  buildKioQuery,
  buildQuery
}