'use strict'

function beautifyJson(body) {
    let result = {}
    Object.keys(body).forEach(key => result[key] = typeof body[key] === 'string' ? body[key].trim() : body[key])
    return result;
}

module.exports = {
    beautifyJson
}